﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market
{
    internal class StudentDiscount : DiscontStrategy
    {
        public override double ApplyDiscount(double amount)
        {
            Console.WriteLine("Student discount is accepted");
            double sub = (amount * 15) / 100;
            return amount - sub;
        }
    }
}
