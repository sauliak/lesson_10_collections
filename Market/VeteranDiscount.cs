﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market
{
    internal class VeteranDiscount : DiscontStrategy
    {
        public override double ApplyDiscount(double amount)
        {
            double sub = (amount * 20) / 100;
            return amount - sub;
        }
    }
}
