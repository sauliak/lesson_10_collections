﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market
{
    internal class DiscontStrategy
    {
        public virtual double ApplyDiscount(double amount)
        {
            Console.WriteLine("Default discount is accepted");
            return amount;
        }
    }
}
