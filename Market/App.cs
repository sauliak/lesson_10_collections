﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market
{
    public class App
    {
        public static void Run() {
            
            ShppingCard shppingCardStudent = new ShppingCard(new StudentDiscount());
            ShppingCard shppingCardDefault = new ShppingCard(new DiscontStrategy());
            shppingCardStudent.Calculate(5000);
            shppingCardDefault.Calculate(8000);
        }
    }
}
