﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market
{
    internal class ShppingCard
    {
        private readonly DiscontStrategy discontStrategy;

        public ShppingCard(DiscontStrategy discontStrategy)
        {
            this.discontStrategy = discontStrategy;
        }

        public double Calculate(double amout)
        {
            return discontStrategy.ApplyDiscount(amout);
        }
    }
}
