﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents.HW
{
    internal class PriceMonitor
    {
        private Random _random = new Random();
        private Action<int> _showPrice;

        public PriceMonitor(Action<int> showPrice)
        {
            _showPrice = showPrice;
        }

        public void GenerateAndShowPrice(int min, int max)
        {
            int price = _random.Next(min, max);
            _showPrice(price);
        }
    }
}
