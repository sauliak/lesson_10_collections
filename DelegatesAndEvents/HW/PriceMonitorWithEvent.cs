﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents.HW
{
    internal class EventPriceMonitor 
    {
        public delegate void PriceFellHandler(object sender, int price);
        public event PriceFellHandler PriceFell;
        private readonly Action<int> _showPrice;
        private readonly Random _random = new Random();

        public EventPriceMonitor(Action<int> showPrice)
        {
            _showPrice = showPrice;
        }

        public void GeneratePrice()
        {
            var price = _random.Next(30000, 100000);
            _showPrice(price);

            if (price < 50000)
            {
                PriceFell?.Invoke(this, price);
            }
        }
    }
}
