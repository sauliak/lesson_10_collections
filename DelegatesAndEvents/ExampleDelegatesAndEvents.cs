﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents
{
    internal class ExampleDelegatesAndEvents
    {
    }

    class Publisher
    {
        public event Action<string> Notify; // Событие

        public void Process()
        {
            Notify?.Invoke("Процесс завершен!");
        }
    }

    class Subscriber
    {
        static void Main()
        {
            var publisher = new Publisher();
            publisher.Notify += HandleNotification; // Подписка на событие

            publisher.Process();
        }

        static void HandleNotification(string message)
        {
            Console.WriteLine(message);
        }
    }  
}
