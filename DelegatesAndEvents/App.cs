﻿using DelegatesAndEvents.EventObserver;
using DelegatesAndEvents.HW;
using DelegatesAndEvents.Observer;
using DelegatesAndEvents.Subsriber;

namespace DelegatesAndEvents
{
    class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
    public class App
    {
        public static void ShowPrice(int price)
        {
            Console.WriteLine($"Цена жилья: {price}");
        }

        public static void OnPriceFell(object sender, int price)
        {
            Console.WriteLine($"Внимание! Цена упала ниже {price}");
        }

        public static void Run()
        {
            List<Student> students = new List<Student>
        {
            new Student { Name = "John", Age = 15 },
            new Student { Name = "Mary", Age = 12 },
            new Student { Name = "Tom", Age = 15 },
            new Student { Name = "Mike", Age = 12 }
        };

            var groupedByAge = students.GroupBy(s => s.Age);

            foreach (var group in groupedByAge)
            {
                Console.WriteLine($"Age: {group.Key}");
                foreach (var student in group)
                {
                    Console.WriteLine($"    Name: {student.Name}");
                }
            }

            var groupedByAge_ = from s in students
                                group s by s.Age;
        }

    }
}