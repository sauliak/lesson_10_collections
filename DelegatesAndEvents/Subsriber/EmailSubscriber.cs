﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents.Subsriber
{
    internal class EmailSubscriber : ISubscriber
    {
        private readonly string email;

        public EmailSubscriber(string email)
        {
            this.email = email;
        }

        public void Notify(string edition)
        {
            Console.WriteLine($"Email to {email}: New Magazine Edition: {edition} released");
        }
    }
}
