﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents.Subsriber
{
    internal class SMSSubscriber : ISubscriber
    {
        private readonly string phoneNumber;

        public SMSSubscriber(string phoneNumber)
        {
            this.phoneNumber = phoneNumber;
        }

        public void Notify(string edition)
        {
            Console.WriteLine($"Sms to {phoneNumber}: New Magazine Edition: {edition} released");
        }
    }
}
