﻿using DelegatesAndEvents.Observer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents.EventObserver
{
    internal class MagazinEventPublisher
    {
        public delegate void NewEditionReleasedHandler(string edition);

        public event NewEditionReleasedHandler OnNewEditionReleased;

        public void NewMagazineRelease(string edition)
        {
            Console.WriteLine($"Magazine publisher: Released: {edition}");
            OnNewEditionReleased?.Invoke(edition);
        }
    }
}
