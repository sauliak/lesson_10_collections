﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DelegatesAndEvents.Subsriber;

namespace DelegatesAndEvents.Observer
{
    internal class MagazinePatternPublisher
    {
        private readonly List<ISubscriber> subscribers = new List<ISubscriber>();

        public MagazinePatternPublisher Subscribe(ISubscriber subscriber)
        {
            subscribers.Add(subscriber);
            return this;
        }

        public void Unsubscribe(ISubscriber subscriber)
        {
            if (subscribers.Remove(subscriber))
            {
                Console.WriteLine("subscriber was removed");
            }
            else
            {
                Console.WriteLine("No such subscriber");
            }
        }

        public void NewMagazineRelease(string edition)
        {
            foreach (var item in subscribers)
            {
                item.Notify(edition);
            }
        }
    }
}
