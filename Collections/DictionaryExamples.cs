﻿using System.Collections.Generic;
using System.Reflection;

namespace Collections
{
    public class DictionaryExamples
    {
        public class ConstantHashObject
        {
            public string Value { get; }

            public ConstantHashObject(string value)
            {
                Value = value;
            }

            public override int GetHashCode() => 1;

            public override string ToString() => Value;
        }


        public static void ExampleBucketEntriesWithSameHash()
        {
            Dictionary<ConstantHashObject, string> dictionary = new Dictionary<ConstantHashObject, string>
        {
            { new ConstantHashObject("one"), "one" },
            { new ConstantHashObject("two"), "two" },
            { new ConstantHashObject("three"), "three" }
        };

            // Получаем тип Dictionary
            Type dictionaryType = dictionary.GetType();

            // Получаем поле _buckets
            FieldInfo bucketsField = dictionaryType.GetField("_buckets", BindingFlags.NonPublic | BindingFlags.Instance);
            int[] buckets = bucketsField.GetValue(dictionary) as int[];

            // Получаем поле _entries
            FieldInfo entriesField = dictionaryType.GetField("_entries", BindingFlags.NonPublic | BindingFlags.Instance);
            Array entries = entriesField.GetValue(dictionary) as Array;

            // Получаем индекс бакета для нашего константного хэш-кода
            int bucketIndex = 1 % buckets.Length; // 1 - это значение хэш-кода

            int entryIndex = buckets[bucketIndex] - 1; // В .NET индексы в buckets смещены на 1
            while (entryIndex >= 0)
            {
                var entry = entries.GetValue(entryIndex);
                Console.WriteLine($"Entry: {entry}");

                FieldInfo nextField = entry.GetType().GetField("next", BindingFlags.Public | BindingFlags.Instance);
                entryIndex = (int)nextField.GetValue(entry);

                if (entryIndex >= 0)
                    Console.WriteLine("Next is not null, there is chaining");
            }

        }

        public static void ExampleBucketEntriesWithDiffHash<TKey, TValue>(Dictionary<TKey, TValue> dictionary)
        {
            // 1. Получение типа словаря и типа Entry.
            Type dictionaryType = dictionary.GetType();

            // 2. Получение поля buckets и entries.
            FieldInfo bucketsField = dictionaryType.GetField("_buckets", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo entriesField = dictionaryType.GetField("_entries", BindingFlags.NonPublic | BindingFlags.Instance);

            // 3. Получение значений buckets и entries.
            int[] buckets = (int[])bucketsField.GetValue(dictionary);
            Array entriesArray = (Array)entriesField.GetValue(dictionary);

            // 4. Вывод содержимого buckets и entries.
            Console.WriteLine("Buckets:");
            for (int i = 0; i < buckets.Length; i++)
            {
                Console.WriteLine($"Bucket {i}: Entry index: {buckets[i] -1}");
            }

            Console.WriteLine("Entries:");

            for (int i = 0; i < entriesArray.Length; i++)
            {
                object entry = entriesArray.GetValue(i);

                Type entryType = entry.GetType();
                // Получение и вывод значений key и value каждого Entry.
                FieldInfo keyField = entryType.GetField("key", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                FieldInfo hashCodeField = entryType.GetField("hashCode", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                FieldInfo valueField = entryType.GetField("value", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                FieldInfo nextField = entryType.GetField("next", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                object key = keyField.GetValue(entry);
                object value = valueField.GetValue(entry);
                object hashCode = hashCodeField.GetValue(entry);
                object next = nextField.GetValue(entry);

                Console.WriteLine($"Entry {i}: Key: {key}, Value: {value}, hashCode {hashCode}, next: {next}");
            }
        }
    }
}
