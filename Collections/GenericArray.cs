﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    public class GenericArray<T> : IEnumerable<T>
    {
        private T[] array;

        public GenericArray()
        {
            array = new T[0];
        }

        public GenericArray<T> Add(T item)
        {
            Array.Resize(ref array, array.Length + 1);
            array[^1] = item;
            return this;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= array.Length) throw new IndexOutOfRangeException();
            for (int i = index; i < array.Length - 1; i++)
            {
                array[i] = array[i + 1];
            }
            Array.Resize(ref array, array.Length - 1);

        }

        public T Get(int index)
        {
            if (index < 0 || index >= array.Length) throw new IndexOutOfRangeException();
            return array[index];
        }

        public GenericArray<T> Where(Predicate<T> predicat)
        {
            GenericArray<T> result = new();
            foreach (var item in array)
            {
                if (predicat(item))
                {
                    result.Add(item);
                }
            }

            return result;
        }

        public GenericArray<TResult> Select<TResult>(Func<T, TResult> selector)
        {
            GenericArray<TResult> result = new();
            foreach (var item in array)
            {
                result.Add(selector(item));
            }

            return result;
        }

        public bool Any(Predicate<T> predicate)
        {
            foreach (var item in array)
            {
                if (predicate(item))
                {
                    return true;
                }
            }

            return false;
        }

        public TResult Aggregate<TResult>(TResult seed, Func<TResult, T, TResult> aggregator)
        {
            TResult result = seed;
            foreach (var item in array)
            {
                result = aggregator(result, item);
            }

            return result;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new GenericArrayEnumerator<T>(array);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Length => array.Length;

        private class GenericArrayEnumerator<T> : IEnumerator<T>
        {

            private T[] array;
            private int position = -1;

            public GenericArrayEnumerator(T[] array)
            {
                this.array = array;
            }

            public T Current
            {
                get
                {
                    return array[position];
                }
            }

            object IEnumerator.Current => Current;

            public void Dispose()
            {}

            public bool MoveNext()
            {
                if (position < array.Length - 1)
                {
                    position++;
                    return true;
                }
                return false;
            }

            public void Reset()
            {
                position = -1;
            }
        }
    }
}
