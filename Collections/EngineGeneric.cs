﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    public abstract class Engine
    {
        public abstract void Refuel();
        public abstract void Start();
    }

    public class DieselEngine : Engine
    {
        public override void Refuel()
        {
            Console.WriteLine("Refueling with diesel fuel");
        }

        public override void Start()
        {
            Console.WriteLine("Diesel engine started");
        }
    }

    public class PetrolEngine : Engine
    {
        public override void Refuel()
        {
            Console.WriteLine("Refueling with petrol");
        }

        public override void Start()
        {
            Console.WriteLine("Petrol engine started");
        }
    }

    public class ElectricEngine : Engine
    {
        public override void Refuel()
        {
            Console.WriteLine("Charging the battery");
        }

        public override void Start()
        {
            Console.WriteLine("Electric engine started");
        }
    }

    public class Car<T> where T : Engine, new()
    {
        private readonly T engine;

        public Car()
        {
            engine = new T();
        }

        public void Move()
        {

            engine.Start();
            Console.WriteLine("The car is moving");
        }

        public void Refuel()
        {
            engine.Refuel();
        }
    }
}
