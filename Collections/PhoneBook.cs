﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    public class PhoneBook : Dictionary<int, Contact>
    {
        public static int Price {
            get => Price;
            set => Price = value;
        }

        public void Add(int phone, string firstName, string lastName) {
            this[phone] = new Contact(firstName, lastName);
        }
    }

    public class Contact
    {
        public Contact(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public string FirstName { get; private set; }
        public string LastName { get; private set; }


        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
