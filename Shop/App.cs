﻿using Shop.DiscountStrtegies;
using Shop.Logger;
using Shop.Models;
using Shop.PaymentServices;
using Shop.PaymentStrategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    internal class App
    {
        public static void RunShop()
        {
            var products = new List<Product>
            {
                new Product("Apple", 1.2M),
                new Product("Banana", 0.8M),
                new Product("Cherry", 2.0M) 
            }.ToArray();

            // Create shopping cart with default discount strategy
            var cart = new ShoppingCart(products);
            // Create discount strategies
            var percentageDiscount = new PercentageDiscount(10);


            cart.SetDiscountStrategy(percentageDiscount);
            decimal totalWithPercentageDiscount = cart.CalculateTotal();


            Console.WriteLine($"Total with percentage discount: {totalWithPercentageDiscount}");

            // Debit card data generation 
            string testCardNumber = "4111111111111111"; 
            string testCardHolderName = "John Doe";
            DateTime testExpiryDate = DateTime.Now.AddYears(2); // will be expired in 2 years

            string testCVV = "123"; // CVV example

            ILogger logger = new FileLogger();
            // Process payment
            IPaymentStrategy paymentStrategy = new CardPayment(testCardNumber, testCardHolderName, testExpiryDate, testCVV);
            PaymentProcessor paymentProcessor = new PaymentProcessor(paymentStrategy, logger);
            paymentProcessor.ProcessPayment(totalWithPercentageDiscount);
        }
    }
}
