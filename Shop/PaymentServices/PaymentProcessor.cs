﻿using Shop.Logger;
using Shop.PaymentStrategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.PaymentServices
{
    internal class PaymentProcessor
    {
        private readonly IPaymentStrategy paymentStrategy;
        private readonly ILogger logger;

        public PaymentProcessor(IPaymentStrategy paymentStrategy, ILogger logger)
        {
            this.paymentStrategy = paymentStrategy;
            this.logger = logger;
        }

        public void ProcessPayment(decimal amount)
        {
            try
            {
                logger.Log($"Attempting to process payment of {amount}.");
                paymentStrategy.ProcessPayment(amount);
                logger.Log($"Successfully processed payment of {amount}.");
            }
            catch (Exception ex)
            {
                logger.Log($"Failed to process payment of {amount}. Error: {ex.Message}");
            }
        }
    }
}
