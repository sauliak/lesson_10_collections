﻿using Shop.Models;

namespace Shop
{
    public class Store
    {
        public List<Product> Products { get; }

        public Store()
        {
            Products = new List<Product>
        {
            new Product("Laptop", 1000),
            new Product("Phone", 500),
            new Product("TV", 1500),
            new Product("Shoes", 100),
            new Product("Book", 20),
            new Product("Watch", 200),
            new Product("Camera", 700),
            new Product("Headphones", 300),
            new Product("Bag", 50),
            new Product("Bicycle", 400)
        };
        }
    }
}
