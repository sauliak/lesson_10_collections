﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.DiscountStrtegies
{
    public class VeteranDiscountPercentage : PercentageStrategy
    {
        public VeteranDiscountPercentage(double discountPercentage) : base(discountPercentage)
        {
        }

        public override double ApplyDiscount(double originalPrice)
        {
            return base.ApplyDiscount(originalPrice) - 10;
        }
    }
}
