﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.DiscountStrtegies
{
    public class PercentageDiscount : DiscountStrategy
    {
        protected readonly decimal discountPercentage;

        public PercentageDiscount(decimal discountPercentage)
        {
            this.discountPercentage = discountPercentage;
        }

        public override decimal ApplyDiscount(decimal originalPrice)
        {
            return originalPrice * (discountPercentage / 100);
        }
    }
}
