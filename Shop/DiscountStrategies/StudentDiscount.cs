﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.DiscountStrtegies
{
    public class StudentDiscount : DiscountStrategy
    {
        public override double ApplyDiscount(double originalPrice)
        {
            return originalPrice * 0.9;
        }
    }

}
