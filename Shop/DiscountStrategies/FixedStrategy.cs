﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.DiscountStrtegies
{
    public class FixedDiscount : DiscountStrategy
    {
        protected readonly decimal fixedAmout;

        public FixedDiscount(decimal fixedAmout)
        {
            this.fixedAmout = fixedAmout;
        }

        public override decimal ApplyDiscount(decimal originalPrice)
        {
            return originalPrice - fixedAmout;
        }
    }
}
