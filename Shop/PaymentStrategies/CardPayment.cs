﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.PaymentStrategies
{
    internal class CardPayment : IPaymentStrategy
    {
        private readonly string cardNumber;
        private readonly string cardHolderName;
        private readonly DateTime expiryDate;
        private readonly string cvv;

        public CardPayment(string cardNumber, string cardHolderName, DateTime expiryDate, string cvv)
        {
            this.cardNumber = cardNumber;
            this.cardHolderName = cardHolderName;
            this.expiryDate = expiryDate;
            this.cvv = cvv;
        }


        private bool VerifyCardDetails() {
            /*
            Check:
             cardNumber
             cardHolderName
             expiryDate
             cvv
             */
            return true;
        }


        public void ProcessPayment(decimal amount)
        {
            VerifyCardDetails();
            Console.WriteLine($"Payment {amount} is being processed via debit card");
        }
    }
}
