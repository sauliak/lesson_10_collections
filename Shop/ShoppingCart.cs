﻿using Shop.DiscountStrtegies;
using Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    public class ShoppingCart
    {
        private DiscountStrategy _discountStrategy;
        private readonly Product[] products;

        public ShoppingCart(Product[] products)
        {
            _discountStrategy = new DiscountStrategy(); // default (no discount)
            this.products = products;
        }

        public void SetDiscountStrategy(DiscountStrategy discountStrategy)
        {
            _discountStrategy = discountStrategy;
        }

        public decimal CalculateTotal()
        {
            decimal total = 0.0M;
            foreach (var item in products)
            {
                total += _discountStrategy.ApplyDiscount(item.Price)
            }
            return total;
        }
    }
}
