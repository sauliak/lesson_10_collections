﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Logger
{
    internal class FileLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine($"{message} logged into a logfile");

        }
    }
}
