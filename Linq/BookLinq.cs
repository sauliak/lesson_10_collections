﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public int Year { get; set; }
        public int Price { get; set; }
    }
    /*
a. Найдите все книги, написанные до 2000 года и отсортируйте их по убыванию года выпуска.

b. Найдите самую дорогую книгу каждого автора.

c. Подсчитайте среднюю стоимость книг, выпущенных после 2010 года.

d. Выведите список авторов (без дубликатов) у которых есть хотя бы одна книга в списке.

e. Найдите книгу с максимальной ценой и выведите ее название, автора и цену.

f. Выведите количество книг, цена которых ниже средней цены всех книг в списке.
     */


    public class BookLinq
    {
        static List<Book> books = new List<Book>
        {
            new Book { Title = "Book1", Author = "Author1", Year = 1999, Price = 500 },
            new Book { Title = "Book2", Author = "Author2", Year = 2010, Price = 1500 },
            new Book { Title = "Book3", Author = "Author1", Year = 2012, Price = 2000 },
            new Book { Title = "Book666", Author = "Author2", Year = 2012, Price = 6666 },
        };

        public static void Task()
        {
            //a. Найдите все книги, написанные до 2000 года и отсортируйте их по убыванию года выпуска.
            books.Where(b => b.Year < 2000)
               .OrderByDescending(b => b.Year)
               .ToList()
               .ForEach(b => Console.WriteLine(b.Title));
            Console.WriteLine();
            //b. Найдите самую дорогую книгу каждого автора.
            books.GroupBy(b => b.Author)
                .Select(a => a.OrderByDescending(b => b.Price).First())
                .ToList()
                .ForEach(b => Console.WriteLine($"Titile: {b.Title}, Price: {b.Price}"));
            // c Подсчитайте среднюю стоимость книг, выпущенных после 2010 года.
            Console.WriteLine();
            Console.WriteLine(books.Where(b => b.Year > 2010).Average(b => b.Price));
            //d. Выведите список авторов не повторяющихся
            books.Select(b => b.Author).Distinct();
            //f. Выведите количество книг, цена которых ниже средней цены всех книг в списке.
            var averegePriceAllBooks = books.Average(b => b.Price);
            Console.WriteLine($"Average price all books {averegePriceAllBooks}");
            var booksNumber = books.Count(b => b.Price < averegePriceAllBooks);
            Console.WriteLine(booksNumber);
        }
    }
}
