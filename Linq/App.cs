﻿using System.Diagnostics;

namespace Linq
{
    /*
a. Подсчитайте общую прибыль (Total Revenue) для каждого продукта.

b. Найдите дату с максимальными продажами (по количеству).

c. Определите, есть ли дни, когда не было продаж (используйте Except).

d. Получите список уникальных продуктов, которые были проданы в январе.

e. Подсчитайте общее количество каждого проданного продукта.

f. Найдите продукт, который приносит максимальную прибыль за день.
     */
    class Sale
    {
        public string Product { get; set; }
        public DateTime Date { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; } // Price per unit
    }

    public class App
    {
        public static void Run()
        {
            //EmployeeLinq.Task();
            //BookLinq.Task();

            List<Sale> sales = new List<Sale>
            {
                new Sale { Product = "Apple", Date = new DateTime(2023, 1, 10), Quantity = 10, Price = 1.20m },
                new Sale { Product = "Banana", Date = new DateTime(2023, 1, 15), Quantity = 20, Price = 0.80m },
            };

            string[] fruits = { "Grape", "Orange", "Apple" };
            int[] amounts = { 1, 2, 3 };

            var result = fruits.SelectMany(f => amounts, (f, a) => new
            {
                Fruit = f,
                Amount = a
            });
            Debug.WriteLine("Selecting all values from each array, and mixing them:");
            foreach (var o in result)
                Debug.WriteLine(o.Fruit + ", " + o.Amount);
          
        }
    }
}
