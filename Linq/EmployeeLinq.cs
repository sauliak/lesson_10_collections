﻿using System.Threading.Channels;

namespace Linq
{
    public class Employee
    {
        public string Name { get; set; }
        public string Department { get; set; }
        public int Salary { get; set; }

        public override string? ToString()
        {
            return $"Name {Name}, Department {Department}, Salary {Salary}";
        }
    }
    /*
a. Найдите всех сотрудников из отдела "IT" и выведите их имена и зарплаты.

b. Посчитайте среднюю зарплату сотрудников в отделе "HR". 

c. Найдите сотрудника с максимальной зарплатой и выведите его имя, отдел и зарплату.

d. Подсчитайте количество сотрудников в каждом отделе и выведите результат.

e. Подсчитайте и выведите количество отделов, где средняя зарплата превышает 50000.
     */
    public class EmployeeLinq
    {
        public static void Task()
        {
            List<Employee> employees = new List<Employee>
            {
            new Employee { Name = "John", Department = "IT", Salary = 60000 },
            new Employee { Name = "Jane", Department = "HR", Salary = 50000 },
            new Employee { Name = "Mike", Department = "IT", Salary = 55000 },
            new Employee { Name = "Grisha", Department = "DEVOPS", Salary = 55000 },
            new Employee { Name = "Nikita", Department = "DEVOPS", Salary = 10000 },
            new Employee { Name = "Jordan", Department = "IT", Salary = 55000 },
            new Employee { Name = "Gena", Department = "HR", Salary = 66000 },
            };


            //a
            var itEmplyees = employees.Where(e => e.Department == "IT").ToList();
            itEmplyees.ForEach(e => Console.WriteLine($"Name {e.Name}, Salary {e.Salary}"));
            //b
            var averageSalary = employees.Where(e => e.Department == "HR").Average(e => e.Salary);
            Console.WriteLine("Average salary in HR department: " + averageSalary);
            Console.WriteLine();
            //c Найдите сотрудника с максимальной зарплатой и выведите его имя, отдел и зарплату.
            var maxSalaryEmployee = employees.OrderByDescending(e => e.Salary).FirstOrDefault();
            Console.WriteLine("maxSalaryEmployee: " + maxSalaryEmployee);
            Console.WriteLine();
            // Подсчитайте количество сотрудников в каждом отделе и выведите результат.
            var countOfEmployeesInEachDempartment = employees
                .GroupBy(e => e.Department)
                .Select(e => new { Department = e.Key, Count = e.Count() });
            foreach (var item in countOfEmployeesInEachDempartment)
            {
                Console.WriteLine($"Dep: {item.Department}, Count: {item.Count}");
            }
            Console.WriteLine();
            //Подсчитайте и выведите количество отделов, где средняя зарплата превышает 50000.
            var moreThan50kCount = employees.GroupBy(e => e.Department)
                .Where(s => s.Average(e => e.Salary) > 50000).Count();
            Console.WriteLine("More than 50k count: " + moreThan50kCount);
        }
    }
}