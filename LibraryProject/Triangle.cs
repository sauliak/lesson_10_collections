﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryProject
{

    public abstract class Triangle
    {
        public abstract double CalculateArea();
    }

    // Равносторонний треугольник
    public class EquilateralTriangle : Triangle
    {
        public double Side { get; }

        public EquilateralTriangle(double side)
        {
            Side = side;
        }

        public override double CalculateArea()
        {
            return (Math.Pow(Side, 2) * Math.Sqrt(3)) / 4;
        }
    }

    // Равнобедренный треугольник
    public class IsoscelesTriangle : Triangle
    {
        public double Base { get; }
        public double Height { get; }

        public IsoscelesTriangle(double b, double h)
        {
            Base = b;
            Height = h;
        }

        public override double CalculateArea()
        {
            return (Base * Height) / 2;
        }
    }

    // Прямоугольный треугольник
    public class RightTriangle : Triangle
    {
        public double LegA { get; }
        public double LegB { get; }

        public RightTriangle(double a, double b)
        {
            LegA = a;
            LegB = b;
        }

        public override double CalculateArea()
        {
            return (LegA * LegB) / 2;
        }
    }

    // Произвольный треугольник
    public class ArbitraryTriangle : Triangle
    {
        public double SideA { get; }
        public double SideB { get; }
        public double SideC { get; }

        public ArbitraryTriangle(double a, double b, double c)
        {
            SideA = a;
            SideB = b;
            SideC = c;
        }

        public override double CalculateArea()
        {
            double p = (SideA + SideB + SideC) / 2;
            return Math.Sqrt(p * (p - SideA) * (p - SideB) * (p - SideC));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Triangle> triangles = new List<Triangle>
        {
            new EquilateralTriangle(5),
            new IsoscelesTriangle(5, 7),
            new RightTriangle(3, 4),
            new ArbitraryTriangle(3, 4, 5)
        };

            foreach (var triangle in triangles)
            {
                Console.WriteLine($"Площадь {triangle.GetType().Name}: {triangle.CalculateArea()}");
            }
        }
    }
}
