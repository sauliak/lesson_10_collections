﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryProject
{
    internal class PrivateProtectedClassExample
    {
        public class OuterClass
        {
            // Вложенный класс с модификатором private protected
            private protected class NestedClass
            {
                public void ShowMessage()
                {
                    Console.WriteLine("This is a private protected nested class.");
                }
            }


        }

        // Производный класс в той же сборке
        public class DerivedClass : OuterClass
        {
            public void AccessNestedClass()
            {
                // Создаем экземпляр вложенного класса
                NestedClass nested = new NestedClass();
                nested.ShowMessage();
            }
        }

        // Класс в той же сборке, но не являющийся производным от OuterClass
        public class AnotherClass
        {
            public void TryToAccessNestedClass()
            {
                // Следующая строка вызовет ошибку компиляции,
                // так как класс NestedClass является private protected
                // NestedClass nested = new NestedClass();
            }
        }
    }
}
