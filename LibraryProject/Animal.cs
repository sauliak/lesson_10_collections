﻿namespace LibraryProject
{
    public class Animal
    {
        internal string InternalName { get; set; } // доступно только внутри этой сборки

        public Animal(string name)
        {
            this.InternalName = name;
            //BaseClass overrideAndNew = new BaseClass();
        }
    }

    //internal class InternalBase
    //{
    //    public void SayHello()
    //    {
    //        Console.WriteLine("Hello from InternalBase");
    //    }
    //}

    //// Публичный производный класс
    //public class PublicDerived : InternalBase
    //{
    //    public void SayHi()
    //    {
    //        Console.WriteLine("Hi from PublicDerived");
    //    }
    //}

    public interface ISomething
    {
        void HelloWorld();
    }

    internal class OldParent : ISomething
    {
        public void HelloWorld() { Console.WriteLine("Hello World!"); }
        protected void SayHi() { Console.WriteLine(); }
    }

    public class OldChild : ISomething
    {
        OldParent _oldParent = new OldParent();

        public void HelloWorld() { _oldParent.HelloWorld(); }
    }
}