﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryProject
{
    public class BaseClass
    {
        public BaseClass() { Console.WriteLine("Base class"); }
        public void ShowMessage()
        {
            Console.WriteLine("Message from BaseClass");
        }

        public virtual void ShowVirtualMessage()
        {
            Console.WriteLine("Virtual message from BaseClass");
        }
    }

    public class DerivedClass : BaseClass
    {
        // Использование ключевого слова new для явного указания на скрытие метода
        public new void ShowMessage()
        {
            Console.WriteLine("Message from DerivedClass");
        }

        // Использование ключевого слова override для переопределения метода
        public override void ShowVirtualMessage()
        {
            Console.WriteLine("Virtual message from DerivedClass");
        }
    }

    class Example
    {
        static void Run(string[] args)
        {
            BaseClass baseClass = new BaseClass();
            DerivedClass derivedClass = new DerivedClass();
            BaseClass upcasted = derivedClass;

            Console.WriteLine("Using BaseClass object:");
            baseClass.ShowMessage();  // Output: "Message from BaseClass"
            baseClass.ShowVirtualMessage();  // Output: "Virtual message from BaseClass"

            Console.WriteLine("\nUsing DerivedClass object:");
            derivedClass.ShowMessage();  // Output: "Message from DerivedClass"
            derivedClass.ShowVirtualMessage();  // Output: "Virtual message from DerivedClass"

            Console.WriteLine("\nUsing Upcasted object:");
            upcasted.ShowMessage();  // Output: "Message from BaseClass"
            upcasted.ShowVirtualMessage();  // Output: "Virtual message from DerivedClass"
        }
    }
}
